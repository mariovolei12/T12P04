package t12p04;

import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelo.ConexionBD;
import modelo.Curso;

public class panelAltaCurso extends javax.swing.JPanel {
    
    private ConexionBD bd;
    private DefaultComboBoxModel comboBoxModel;
    private String[] items;
    private boolean editar;
    private ICallBack icb;
    
    public panelAltaCurso(ConexionBD bd) {
        initComponents();
        this.editar=false;
        this.icb=null;
        this.bd=bd;
        this.items=new String[]{"","Programado","Realizandose","Finalizado"};
        this.comboBoxModel= new DefaultComboBoxModel(items);
        CBoxEstado.setModel(comboBoxModel);
    }
    
    public void mostrar(ICallBack icb){
        this.editar=false;
        this.icb=icb;
        txtID.setEditable(true);
        txtID.setBackground(Color.WHITE);
        Cancelar.setText("Cancelar");
        jLabel4.setText("ALTA CURSO");
        txtID.setText("");
        txtTitulo.setText("");
        txtHoras.setText("");
        txtFechIni.setText("");
        txtFechFin.setText("");
        RButtonPresencial.setSelected(true);
        RButtonTelematico.setSelected(false);
        
        setVisible(true);
    }
    
    public void editar(Curso c, ICallBack icb){
        this.editar=true;
        
        this.icb=icb;
        txtID.setText(String.valueOf(c.getId()));
        if (editar){
            txtID.setEditable(false);
            txtID.setBackground(Color.GRAY);
            Cancelar.setText("Volver");
            jLabel4.setText("EDITAR CURSO");
        }
        txtTitulo.setText(c.getTitulo());
        txtHoras.setText(String.valueOf(c.getHoras()));
        txtFechIni.setText(c.getFecIni());
        txtFechFin.setText(c.getFecFin());
        if (c.getModalidad()=='P') {
            RButtonPresencial.setSelected(true);
        } else if(c.getModalidad()=='T'){
            RButtonTelematico.setSelected(true);
        }
        if (c.getEstado()!=null) {
            switch (c.getEstado().toString().charAt(0)) {
                case 'P':
                    CBoxEstado.setSelectedIndex(1);
                    break;
                case 'R':
                    CBoxEstado.setSelectedIndex(2);
                    break;
                case 'F':
                    CBoxEstado.setSelectedIndex(3);
                    break;
                default:
                    CBoxEstado.setSelectedIndex(0);
            }
        }
        
        setVisible(true);
    }
    
    
    public boolean datosObligatorios(){
        if (txtID.getText().equals("") || txtTitulo.getText().equals("") ) {
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupModalidad = new javax.swing.ButtonGroup();
        txtID = new javax.swing.JTextField();
        txtTitulo = new javax.swing.JTextField();
        txtHoras = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        Cancelar = new javax.swing.JButton();
        Aceptar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        RButtonPresencial = new javax.swing.JRadioButton();
        RButtonTelematico = new javax.swing.JRadioButton();
        CBoxEstado = new javax.swing.JComboBox<>();
        txtFechIni = new javax.swing.JFormattedTextField();
        txtFechFin = new javax.swing.JFormattedTextField();

        setMinimumSize(new java.awt.Dimension(600, 300));
        setPreferredSize(new java.awt.Dimension(600, 300));

        jLabel1.setText("ID curso:");

        jLabel2.setText("Titulo:");

        jLabel3.setText("Horas:");

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setText("ALTA CURSO");

        Cancelar.setText("Cancelar");
        Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelarActionPerformed(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });

        jLabel5.setText("F. Inicio:");

        jLabel6.setText("F. Fin:");

        jLabel7.setText("Modalidad:");

        jLabel8.setText("Estado:");

        buttonGroupModalidad.add(RButtonPresencial);
        RButtonPresencial.setText("Presencial");

        buttonGroupModalidad.add(RButtonTelematico);
        RButtonTelematico.setText("Telemático");

        CBoxEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        txtFechIni.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        txtFechFin.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel5)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel3))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(26, 26, 26)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(RButtonTelematico)
                                            .addComponent(RButtonPresencial)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtFechIni, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(txtHoras)
                                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(113, 113, 113)
                                        .addComponent(jLabel4)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(CBoxEstado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtFechFin, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(291, 291, 291)
                        .addComponent(Cancelar)
                        .addGap(33, 33, 33)
                        .addComponent(Aceptar)))
                .addContainerGap(130, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtFechFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(CBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(43, 43, 43)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Aceptar)
                            .addComponent(Cancelar)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtFechIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(RButtonPresencial))
                        .addGap(3, 3, 3)
                        .addComponent(RButtonTelematico)
                        .addGap(41, 41, 41)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_CancelarActionPerformed

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        try {
            if (!datosObligatorios()) {
            JOptionPane.showMessageDialog(this,
                    "Hay campos vacios",
                    "Error",
                    JOptionPane.INFORMATION_MESSAGE);
            } else {
                Curso c = new Curso();
                c.setId(Integer.parseInt(txtID.getText()));
                c.setTitulo(txtTitulo.getText());
                if (!txtHoras.getText().equals("")) {
                    c.setHoras(Double.parseDouble(txtHoras.getText()));
                }

                c.setFecIni(txtFechIni.getText());
                c.setFecFin(txtFechIni.getText());

                if (RButtonPresencial.isSelected()) {
                    c.setModalidad('P');
                } else if (RButtonTelematico.isSelected()) {
                    c.setModalidad('T');
                }

                if (CBoxEstado.getSelectedItem().equals("Realizandose")) {
                    c.setEstado(Curso.ESTADO.Realizandose);
                } else if (CBoxEstado.getSelectedItem().equals("Programado")) {
                    c.setEstado(Curso.ESTADO.Programado);
                } else if (CBoxEstado.getSelectedItem().equals("Finalizado")) {
                    c.setEstado(Curso.ESTADO.Finalizado);
                } else {
                    c.setEstado(null);
                }

                try {
                    if (editar) {
                        c.actualizar(bd);
                        JOptionPane.showMessageDialog(this,
                                "Actualizacion de curso correcta!!\n"
                                + " ID: " + c.getId(),
                                "Editar curso",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (icb != null) {
                            icb.actualizarBusqueda();
                        }
                    } else {
                        c.altaCurso(bd);
                        JOptionPane.showMessageDialog(this,
                                "Curso añadido correctamente\n"
                                + " ID: " + c.getId(),
                                "Alta curso",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (icb != null) {
                            icb.actualizarBusqueda();
                        }
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this,
                            "Error!!\n" + e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            
            
        } catch (Exception e){
            JOptionPane.showMessageDialog(this, 
                    "Error!!\n"+e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }       
    }//GEN-LAST:event_AceptarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Aceptar;
    private javax.swing.JComboBox<String> CBoxEstado;
    private javax.swing.JButton Cancelar;
    private javax.swing.JRadioButton RButtonPresencial;
    private javax.swing.JRadioButton RButtonTelematico;
    private javax.swing.ButtonGroup buttonGroupModalidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JFormattedTextField txtFechFin;
    private javax.swing.JFormattedTextField txtFechIni;
    private javax.swing.JTextField txtHoras;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables
}
