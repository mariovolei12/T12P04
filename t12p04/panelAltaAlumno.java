/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t12p04;

import java.awt.Color;
import javax.swing.JOptionPane;
import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;

/**
 *
 * @author Alumno
 */
public class panelAltaAlumno extends javax.swing.JPanel {

    private ConexionBD bd;
    private ICallBack icb;
    private boolean editar;
    
    public panelAltaAlumno(ConexionBD bd) {
        initComponents();
        this.bd=bd;
        this.icb=null;
        this.editar=false;
    }

    public void mostrar(ICallBack icb){
        this.icb=icb;
        this.editar=false;
        jLabel1.setText("ALTA ALUMNO");
        Cancelar.setText("Cancelar");
        txtIDCurso.setEditable(true);
        txtIDCurso.setBackground(Color.WHITE);
        txtIDCurso.setText("");
        txtDNI.setText("");
        txtNombre.setText("");
        CkBoxEdad.setSelected(false);
        setVisible(true);
    }
    
    public void editar(Alumno a, ICallBack icb){
        this.icb=icb;
        this.editar=true;
        if (editar) {
            jLabel1.setText("EDITAR ALUMNO");
            Cancelar.setText("Volver");
        }
        txtIDCurso.setText(String.valueOf(a.getIdCurso()));
        txtIDCurso.setEditable(false);
        txtIDCurso.setBackground(Color.GRAY);
        txtDNI.setText(a.getDni());
        txtDNI.setEditable(false);
        txtDNI.setBackground(Color.gray);
        txtNombre.setText(a.getNombre());
        if (a.isMayorEdad()) {
            CkBoxEdad.setSelected(true);
        }
        this.setVisible(true);
    }
    
    public boolean datosObligatorios(){
        if (txtIDCurso.getText().equals("") || txtDNI.getText().equals("")) {
            return false;
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtIDCurso = new javax.swing.JTextField();
        txtDNI = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        Cancelar = new javax.swing.JButton();
        Aceptar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        CkBoxEdad = new javax.swing.JCheckBox();

        setMinimumSize(new java.awt.Dimension(450, 300));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("ALTA ALUMNO");

        jLabel2.setText("ID Curso:");

        jLabel3.setText("DNI:");

        jLabel4.setText("Nombre:");

        Cancelar.setText("Cancelar");
        Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelarActionPerformed(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });

        jLabel5.setText("Mayor edad:");

        CkBoxEdad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CkBoxEdadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(txtIDCurso, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                            .addComponent(txtDNI)
                            .addComponent(txtNombre, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(CkBoxEdad)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addComponent(Cancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                        .addComponent(Aceptar)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIDCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(CkBoxEdad))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Cancelar)
                    .addComponent(Aceptar))
                .addContainerGap(65, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_CancelarActionPerformed

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        try {
            if (!datosObligatorios()) {
                JOptionPane.showMessageDialog(this,
                        "Hay campos vacios",
                        "Error",
                        JOptionPane.INFORMATION_MESSAGE);
            }

            Curso c = new Curso();
            c.setId(Integer.parseInt(txtIDCurso.getText()));

            if (!c.existeCurso(bd)) {
                JOptionPane.showMessageDialog(this,
                        "No existe el curso!!",
                        "",
                        JOptionPane.WARNING_MESSAGE);
            } else {
                Alumno a = new Alumno();
                a.setIdCurso(Integer.parseInt(txtIDCurso.getText()));
                a.setDni(txtDNI.getText());
                a.setNombre(txtNombre.getText());
                if (CkBoxEdad.isSelected())
                    a.setMayorEdad(true);
                else
                    a.setMayorEdad(false);
                try {
                    if (editar==false) {
                        a.altaAlumno(bd);
                        JOptionPane.showMessageDialog(this,
                                "Alumno añadido correctamente\n"
                                + "ID Curso: " + a.getIdCurso() + "\n"
                                + "DNI: " + a.getDni() + "\n"
                                + "Nombre: " + a.getNombre() + "\n"
                                + "Mayor edad: " + a.isMayorEdad(),
                                "Alta correcta!!",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (icb != null) {
                            icb.actualizarBusqueda();
                        }
                    }else{
                        a.actualizar(bd);
                        JOptionPane.showMessageDialog(this,
                                "Actualizacion de curso correcta!!\n"
                                + "ID Curso: " + a.getIdCurso() + "\n"
                                + "DNI: " + a.getDni() + "\n"
                                + "Nombre: " + a.getNombre() + "\n"
                                + "Mayor edad: " + a.isMayorEdad(),
                                "Editar alumno",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (icb!=null) {
                            icb.actualizarBusqueda();
                        }
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this,
                            "Error!!\n" + e.getMessage(),
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error!!\n" + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_AceptarActionPerformed

    private void CkBoxEdadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CkBoxEdadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CkBoxEdadActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Aceptar;
    private javax.swing.JButton Cancelar;
    private javax.swing.JCheckBox CkBoxEdad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField txtDNI;
    private javax.swing.JTextField txtIDCurso;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
