package t12p04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;

public class panelBuscadorAlumnos extends javax.swing.JPanel
                                    implements ICallBack {

    private ConexionBD bd;
    private DefaultTableModel tableModel;
    private String[] columnas;
    private panelAltaAlumno panelAA;
    
    public panelBuscadorAlumnos(ConexionBD bd, panelAltaAlumno panelAA) {
        initComponents();
        this.bd=bd;
        this.panelAA=panelAA;
        this.tableModel=new DefaultTableModel();
        this.columnas=new String[]{"Curso","DNI","Nombre","MayorEdad"};
        this.tableModel.setColumnIdentifiers(columnas);
        this.tableAlumnos.setModel(tableModel);
        
    }

    public void mostrar(){
        this.txtCurso.setText("");
        this.txtDNI.setText("");
        Limpiar.doClick();
        this.setVisible(true);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtDNI = new javax.swing.JTextField();
        Buscar = new javax.swing.JButton();
        Limpiar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAlumnos = new javax.swing.JTable();
        Alta = new javax.swing.JButton();
        Baja = new javax.swing.JButton();
        Editar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtCurso = new javax.swing.JTextField();

        setMinimumSize(new java.awt.Dimension(600, 300));

        Buscar.setText("Buscar");
        Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarActionPerformed(evt);
            }
        });

        Limpiar.setText("Limpiar");
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });

        tableAlumnos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableAlumnos);

        Alta.setText("Alta");
        Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AltaActionPerformed(evt);
            }
        });

        Baja.setText("Baja");
        Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BajaActionPerformed(evt);
            }
        });

        Editar.setText("Editar");
        Editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("BUSCADOR DE ALUMNOS");

        jLabel2.setText("Curso:");

        jLabel3.setText("DNI:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(208, 208, 208)
                        .addComponent(jLabel1))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(83, 83, 83)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(Buscar, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(Limpiar, javax.swing.GroupLayout.Alignment.TRAILING)))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(37, 37, 37)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(Alta)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(Baja)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(Editar))))))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Alta)
                            .addComponent(Baja)
                            .addComponent(Editar))
                        .addGap(19, 19, 19))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(Buscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Limpiar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarActionPerformed
        try{
            List<Alumno> tAlumnos=new ArrayList<>();
            Alumno.listadoFiltro(bd, tAlumnos, txtCurso.getText(), txtDNI.getText());
            Collections.sort(tAlumnos);
            
            //Eliminamos filas
            for (int i = tableModel.getRowCount()-1; i >= 0; i--) {
                tableModel.removeRow(i);
            }
            
            //Añadimos filas
            for (Alumno a: tAlumnos){
                Object[] s={a.getIdCurso(),a.getDni(),a.getNombre(),a.isMayorEdad()};
                tableModel.addRow(s);
            }
        } catch (Exception e){
            JOptionPane.showMessageDialog(this,
                        "Error!!\n"+e.getMessage(),
                        "Error", 
                        JOptionPane.ERROR_MESSAGE);
        }
           

    }//GEN-LAST:event_BuscarActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        //Borramos filas
        for (int i = tableModel.getRowCount()-1; i >= 0; i--) {
            tableModel.removeRow(i);
        }
    }//GEN-LAST:event_LimpiarActionPerformed

    private void AltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AltaActionPerformed
        panelAA.mostrar(this);
    }//GEN-LAST:event_AltaActionPerformed

    private void BajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BajaActionPerformed
        if (tableAlumnos.getSelectedRowCount()==1){
            int fila=tableAlumnos.getSelectedRow();
            int curso=Integer.valueOf(tableModel.getValueAt(fila, 0).toString());
            String dni=tableModel.getValueAt(fila, 1).toString();
            Alumno a= new Alumno();
            a.setDni(dni);
            a.setIdCurso(curso);
            
            try{
                int op= JOptionPane.showConfirmDialog(this,
                        "¿Está seguro que desea borrar el alumno seleccionado?",
                        "",
                        JOptionPane.YES_NO_OPTION);
                if (op==JOptionPane.YES_OPTION ) {
                    a.bajaAlumno(bd);
                    Buscar.doClick();
                    
                }
            } catch (Exception e){
                JOptionPane.showMessageDialog(this,
                        "Error!!\n"+e.getMessage(),
                        "Error", 
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_BajaActionPerformed

    private void EditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditarActionPerformed
        if (tableAlumnos.getSelectedRowCount()==1) {
            int fila=tableAlumnos.getSelectedRow();
            int id=Integer.valueOf(tableModel.getValueAt(fila, 0).toString());
            String dni=tableModel.getValueAt(fila, 1).toString();
            try{
                Alumno a=new Alumno();
                a.setIdCurso(id);
                a.setDni(dni);
                a.recuperar(bd);
                panelAA.editar(a, this);
            } catch(Exception e){
                JOptionPane.showMessageDialog(this,
                        "Error!!"+e.getMessage(),
                        "",
                        JOptionPane.ERROR_MESSAGE);
            }
            
        }
    }//GEN-LAST:event_EditarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Alta;
    private javax.swing.JButton Baja;
    private javax.swing.JButton Buscar;
    private javax.swing.JButton Editar;
    private javax.swing.JButton Limpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableAlumnos;
    private javax.swing.JTextField txtCurso;
    private javax.swing.JTextField txtDNI;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actualizarBusqueda() {
        Buscar.doClick();
    }
}
