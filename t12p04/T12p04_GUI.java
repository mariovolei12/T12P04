package t12p04;

import javax.swing.JOptionPane;
import modelo.ConexionBD;

public class T12p04_GUI extends javax.swing.JFrame {
    
    private panelAltaCurso panelAC;
    private panelBajaCurso panelBC;
    private panelListadoCursos panelLC;
    private panelAltaAlumno panelAA;
    private panelBajaAlumno panelBA;
    private panelListadoAlumnos panelLA;
    private panelBuscadorCursos pbc;
    private panelBuscadorAlumnos pba;
    
    private ConexionBD bd;
    
    public T12p04_GUI() {
        initComponents();
        this.setTitle("T12p04");
        bd=new ConexionBD();
        this.setLocationRelativeTo(null); //Con esto el panel aparece en el centro
        
        //Paneles cursos
        panelAC=new panelAltaCurso(bd);
        panelAC.setVisible(false);
        add(panelAC);
        panelBC=new panelBajaCurso(bd);
        panelBC.setVisible(false);
        add(panelBC);
        panelLC=new panelListadoCursos(bd);
        panelLC.setVisible(false);
        add(panelLC);
        pbc=new panelBuscadorCursos(bd,panelAC);
        pbc.setVisible(false);
        add(pbc);
        
        //Paneles alumnos
        panelAA=new panelAltaAlumno(bd);
        panelAA.setVisible(false);
        add(panelAA);
        panelBA=new panelBajaAlumno(bd);
        panelBA.setVisible(false);
        add(panelBA);
        panelLA=new panelListadoAlumnos(bd);
        panelLA.setVisible(false);
        add(panelLA);
        pba=new panelBuscadorAlumnos(bd,panelAA);
        pba.setVisible(false);
        add(pba);
        
        
        
        /* ABRIR CONEXIÓN BD **************************************************/
        try
        {
            System.out.println("Abriendo conexión BD...");
            bd.abrirConexion();
            System.out.println("Conexión abierta correctamente.");
        } catch (Exception e) {
            System.out.println("Error!!\n"+e.getMessage());
            JOptionPane.showMessageDialog(this,
                    "Error!! "+e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        /**********************************************************************/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        menuPrincipal = new javax.swing.JMenu();
        menuPrincipal_Salir = new javax.swing.JMenuItem();
        menuCursos = new javax.swing.JMenu();
        menuCursos_Buscador = new javax.swing.JMenuItem();
        menuCursos_Alta = new javax.swing.JMenuItem();
        menuCursos_Baja = new javax.swing.JMenuItem();
        menuCursos_Listado = new javax.swing.JMenuItem();
        menuAlumnos = new javax.swing.JMenu();
        menuAlumnos_Buscador = new javax.swing.JMenuItem();
        menuAlumnos_Alta = new javax.swing.JMenuItem();
        menuAlumnos_Baja = new javax.swing.JMenuItem();
        menuAlumnos_Listado = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 400));
        setSize(new java.awt.Dimension(500, 400));
        getContentPane().setLayout(new java.awt.FlowLayout());

        menuPrincipal.setText("Principal");

        menuPrincipal_Salir.setText("Salir");
        menuPrincipal_Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPrincipal_SalirActionPerformed(evt);
            }
        });
        menuPrincipal.add(menuPrincipal_Salir);

        menuBar.add(menuPrincipal);

        menuCursos.setText("Cursos");

        menuCursos_Buscador.setText("Buscador");
        menuCursos_Buscador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Buscador);

        menuCursos_Alta.setText("Alta");
        menuCursos_Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Alta);

        menuCursos_Baja.setText("Baja");
        menuCursos_Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Baja);

        menuCursos_Listado.setText("Listado");
        menuCursos_Listado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Listado);

        menuBar.add(menuCursos);

        menuAlumnos.setText("Alumnos");

        menuAlumnos_Buscador.setText("Buscador");
        menuAlumnos_Buscador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumnos_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Buscador);

        menuAlumnos_Alta.setText("Alta");
        menuAlumnos_Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumnos_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Alta);

        menuAlumnos_Baja.setText("Baja");
        menuAlumnos_Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumnos_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Baja);

        menuAlumnos_Listado.setText("Listado");
        menuAlumnos_Listado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumnos_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Listado);

        menuBar.add(menuAlumnos);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuPrincipal_SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPrincipal_SalirActionPerformed
        int op = JOptionPane.showConfirmDialog(this,
                "¿Está seguro que desea cerrar la aplicación?",
                this.getTitle(),
                JOptionPane.YES_NO_OPTION);
        switch (op) {
            case JOptionPane.YES_OPTION:
                /* CERRAR CONEXIÓN BD *****************************************/
                try {
                    System.out.println("Cerrando conexión BD...");
                    bd.cerrarConexion();
                    System.out.println("Conexión cerrada correctamente.");
                } catch (Exception e) {
                    System.out.println("Error!!\n" + e.getMessage());
                    JOptionPane.showMessageDialog(this,
                            "Error!! " + e.getMessage(),
                            "Error", JOptionPane.ERROR_MESSAGE);
                } finally{
                    JOptionPane.showMessageDialog(this,
                            "Hasta la próxima, señor Mario.",
                            this.getTitle(),
                            JOptionPane.INFORMATION_MESSAGE);
                }
                /**
                 * ***********************************************************
                 */
                this.setVisible(false);
                this.dispose(); //Libera el JFrame
                break;
        }
    }//GEN-LAST:event_menuPrincipal_SalirActionPerformed

    private void menuCursos_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCursos_ActionPerformed
        panelAC.setVisible(false);
        panelBC.setVisible(false);
        panelLC.setVisible(false);
        panelAA.setVisible(false);
        panelBA.setVisible(false);
        panelLA.setVisible(false);
        pbc.setVisible(false);
        pba.setVisible(false);
        
        switch (evt.getActionCommand()) {
            case "Alta":
                panelAC.mostrar(null);
                break;
            case "Baja":
                panelBC.mostrar();
                break;
            case "Listado":
                panelLC.mostrar();
                break;
            case "Buscador":
                pbc.mostrar();
                break;
            default:
                throw new AssertionError();
        }
    }//GEN-LAST:event_menuCursos_ActionPerformed

    private void menuAlumnos_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAlumnos_ActionPerformed
        panelAA.setVisible(false);
        panelBA.setVisible(false);
        panelLA.setVisible(false);
        panelAC.setVisible(false);
        panelBC.setVisible(false);
        panelLC.setVisible(false);
        pbc.setVisible(false);
        pba.setVisible(false);
        
        switch (evt.getActionCommand()){
            case "Alta":
                panelAA.mostrar(null);
                break;
            case "Baja":
                panelBA.mostrar();
                break;
            case "Listado":
                panelLA.mostrar();
                break;
            case "Buscador":
                pba.mostrar();
                break;
            default:
                throw new AssertionError();
        }
    }//GEN-LAST:event_menuAlumnos_ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(T12p04_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(T12p04_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(T12p04_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(T12p04_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new T12p04_GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu menuAlumnos;
    private javax.swing.JMenuItem menuAlumnos_Alta;
    private javax.swing.JMenuItem menuAlumnos_Baja;
    private javax.swing.JMenuItem menuAlumnos_Buscador;
    private javax.swing.JMenuItem menuAlumnos_Listado;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuCursos;
    private javax.swing.JMenuItem menuCursos_Alta;
    private javax.swing.JMenuItem menuCursos_Baja;
    private javax.swing.JMenuItem menuCursos_Buscador;
    private javax.swing.JMenuItem menuCursos_Listado;
    private javax.swing.JMenu menuPrincipal;
    private javax.swing.JMenuItem menuPrincipal_Salir;
    // End of variables declaration//GEN-END:variables
}
