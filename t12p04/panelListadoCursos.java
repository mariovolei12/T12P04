package t12p04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.ConexionBD;
import modelo.Curso;

public class panelListadoCursos extends javax.swing.JPanel {

    private ConexionBD bd;
    private DefaultTableModel tableModel;
    String[] columnas;
    
    public panelListadoCursos(ConexionBD bd) {
        initComponents();
        this.bd=bd;
        this.columnas=new String[]{"ID","Titulo","Horas","FecIni","FecFin","Modalidad","Estado"};
        this.tableModel=new DefaultTableModel();
        this.tableModel.setColumnIdentifiers(columnas);
        this.tableCursos.setModel(tableModel);
    }
    
    public void mostrar(){
        
        try{
            List<Curso> tCursos=new ArrayList<>();
            Curso.listadoCursos(bd, tCursos);
            Collections.sort(tCursos);
            //Borramos las filas
            for (int i = tableModel.getRowCount()-1; i >= 0; i--) {
                tableModel.removeRow(i);
            }
            //Añadir las filas
            for (Curso c: tCursos) {
                Object[] s={c.getId(),c.getTitulo(),c.getHoras(),c.getFecIni(),c.getFecFin(),c.getModalidad(),c.getEstado()};
                tableModel.addRow(s);
            }
        } catch (Exception e){
            JOptionPane.showMessageDialog(this, 
                    "Error!!\n"+e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        this.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableCursos = new javax.swing.JTable();

        setMinimumSize(new java.awt.Dimension(600, 300));
        setPreferredSize(new java.awt.Dimension(600, 300));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("LISTADO DE CURSOS");

        tableCursos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tableCursos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 533, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
            .addGroup(layout.createSequentialGroup()
                .addGap(226, 226, 226)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tableCursos;
    // End of variables declaration//GEN-END:variables
}
