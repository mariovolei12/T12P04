package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JOptionPane;

public class Alumno implements Comparable<Alumno>
{
    /* Atributos **************************************************************/

    private int idCurso;
    private String dni;
    private String nombre;
    private boolean mayorEdad;

    /* Constructores **********************************************************/

    public Alumno() {
        idCurso=0;
        dni="";
        nombre="";
        mayorEdad=false;
    }

    /* Métodos getters & setters **********************************************/

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isMayorEdad() {
        return mayorEdad;
    }

    public void setMayorEdad(boolean mayorEdad) {
        this.mayorEdad = mayorEdad;
    }
    
    

    /* Métodos ****************************************************************/

    public boolean existeAlumno(ConexionBD bd) throws Exception {
        try {
            String sql="SELECT count(*) FROM Alumnos WHERE "+
                        "idCurso="+idCurso+" AND "+
                        "dni='"+dni+"'";
            ResultSet rs=bd.getSt().executeQuery(sql);
            rs.next();
            int n=rs.getInt(1);
            if (n>0) return true;
        } catch (SQLException e) {
            throw new Exception("Error existeAlumno()!!",e);
        }
        return false;
    }
    
    public void altaAlumno(ConexionBD bd) throws Exception {
        if (existeAlumno(bd)) throw new Exception("El alumno ya existe en este curso!!");
        try {
            String sql="INSERT INTO Alumnos VALUES ("+
                        idCurso+",'"+
                        dni+"','"+
                        nombre+"',"+
                        mayorEdad+")";
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error altaAlumno()!!",e);
        }
    }

    public void bajaAlumno(ConexionBD bd) throws Exception {
        if (!existeAlumno(bd)) throw new Exception("El alumno no existe en este curso!!");
        try {
            String sql="DELETE FROM Alumnos WHERE "+
                        "idCurso="+idCurso+" AND "+
                        "dni='"+dni+"'";
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error bajaAlumno()!!",e);
        }
    }
    
    public static void listadoAlumnos(ConexionBD bd, List<Alumno> t) throws Exception {
        try {
            String sql="SELECT * FROM Alumnos";
            ResultSet rs=bd.getSt().executeQuery(sql);
            Alumno a;
            while (rs.next()) {
                a=new Alumno();
                a.setIdCurso(rs.getInt("idCurso"));
                a.setDni(rs.getString("dni"));
                a.setNombre(rs.getString("nombre"));
                a.setMayorEdad(rs.getBoolean("mayorEdad"));
                t.add(a);
            }
        } catch (SQLException e) {
            throw new Exception("Error listadoAlumnos()!!",e);
        }
    }
    
    public static void listadoFiltro(ConexionBD bd, List<Alumno> t, String curso, String dni) throws Exception{
        try{
            if (curso.equals("") && dni.equals("")) {
                String sql="SELECT * FROM ALUMNOS";
                listar(bd,t,sql);
            } else if(!curso.equals("") && !dni.equals("")){
                String sql="SELECT * FROM ALUMNOS"
                        + " WHERE idcurso=" +Integer.valueOf(curso)+ ""
                        + " AND dni=" +dni+ "";
                listar(bd,t,sql);
            } else if(!curso.equals("") && dni.equals("")){
                String sql="SELECT * FROM ALUMNOS"
                        + " WHERE idcurso=" +Integer.valueOf(curso)+ "";
                listar(bd,t,sql);
            } else if(curso.equals("") && !dni.equals("")){
                String sql="SELECT * FROM ALUMNOS"
                        + " WHERE dni=" +dni+ "";
                listar(bd,t,sql);
            }
        } catch (Exception e){
            throw new Exception("Error listadoFiltro()!!",e);
        }
    }
    
    public static void listar(ConexionBD bd, List<Alumno> t ,String sql) throws Exception {
        try{
            ResultSet rs = bd.getSt().executeQuery(sql);
            Alumno a;
            
            while (rs.next()) {
                a=new Alumno();
                a.setIdCurso(rs.getInt("idCurso"));
                a.setDni(rs.getString("dni"));
                a.setNombre(rs.getString("nombre"));
                a.setMayorEdad(rs.getBoolean("mayorEdad"));
                t.add(a);
            }
        } catch (SQLException e){
            throw new Exception("Error listar()!!",e);
        }
    }
    
    public Alumno recuperar(ConexionBD bd) throws Exception {
        try{
            String sql="SELECT * FROM ALUMNOS"
                + " WHERE dni="+this.dni+ ""
                + " AND idcurso="+this.idCurso+ "";
            ResultSet rs=bd.getSt().executeQuery(sql);
            while (rs.next()) {
                this.setIdCurso(rs.getInt("idCurso"));
                this.setDni(rs.getString("dni"));
                this.setNombre(rs.getString("nombre"));
                if (rs.getBoolean("mayorEdad")) {
                    this.setMayorEdad(true);
                } else {
                    this.setMayorEdad(false);
                }
                return this;
            }
        } catch (SQLException e){
            throw new Exception("Error recuperar()!!",e);
        }
        return null;
    }
    
    public void actualizar(ConexionBD bd) throws Exception {
        try{
            String sql="UPDATE alumnos"
                + " SET nombre= '" +nombre+ "',"
                + " mayorEdad=" +mayorEdad+ ""
                + " WHERE idCurso=" +idCurso+ ""
                + " AND dni= '" +dni+ "'";
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e){
            throw new Exception("Error actualizar()!!",e);
        }
    }

    @Override
    public int compareTo(Alumno o) {
        return this.getDni().compareToIgnoreCase(o.getDni());
    }
    
}
